package com.company;

import javafx.event.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.geom.Arc2D;
import java.util.HashMap;
import java.util.Stack;
import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        try  {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
        }

        JFrame frm = new JFrame("Calculator");
        frm.setSize(300, 700);
        frm.setLayout(null);

        JTextField input_field = new JTextField();
        input_field.setBounds(20, 100, 240, 30);

        JButton but_plus = new JButton("+");
        but_plus.setBounds(20, 150, 50, 50);

        JButton but_minus = new JButton("-");
        but_minus.setBounds(80, 150, 50, 50);

        JButton but_mul = new JButton("*");
        but_mul.setBounds(140, 150, 50, 50);

        JButton but_div = new JButton("/");
        but_div.setBounds(200, 150, 50, 50);

        JButton but_eq = new JButton("=");
        but_eq.setBounds(80, 210, 110, 50);

        JButton but_clear = new JButton("C");
        but_clear.setBounds(80, 270, 110, 50);

        JButton but_pol = new JButton("To postfix");
        but_pol.setBounds(80, 330, 110, 50);


        JLabel label_1 = new JLabel("");
        label_1.setBounds(20, 70, 50, 30);

        JLabel label_2 = new JLabel("");
        label_2.setBounds(70, 70, 30, 30);

        JLabel label_3 = new JLabel("");
        label_3.setBounds(100, 70, 50, 30);

        JLabel label_4 = new JLabel("");
        label_4.setBounds(150, 70, 30, 30);

        JLabel label_5 = new JLabel("");
        label_5.setBounds(180, 70, 100, 30);

        //===============================================================//

        WindowListener wndCloser = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        frm.addWindowListener(wndCloser);

        //===============================================================//

        ActionListener plus_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (label_1.getText() == "") {
                    label_1.setText(input_field.getText());
                    input_field.setText("");
                    label_2.setText("+");
                } else {
                    label_3.setText(input_field.getText());
                    input_field.setText("");
                }
            }
        };
        but_plus.addActionListener(plus_listener);

        //===============================================================//

        ActionListener minus_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (label_1.getText() == "") {
                    label_1.setText(input_field.getText());
                    input_field.setText("");
                    label_2.setText("-");
                } else {
                    label_3.setText(input_field.getText());
                    input_field.setText("");
                }
            }
        };
        but_minus.addActionListener(minus_listener);

        //===============================================================//

        ActionListener mul_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (label_1.getText() == "") {
                    label_1.setText(input_field.getText());
                    input_field.setText("");
                    label_2.setText("*");
                } else {
                    label_3.setText(input_field.getText());
                    input_field.setText("");
                }
            }
        };
        but_mul.addActionListener(mul_listener);

        //===============================================================//

        ActionListener div_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (label_1.getText() == "") {
                    label_1.setText(input_field.getText());
                    input_field.setText("");
                    label_2.setText("/");
                } else {
                    label_3.setText(input_field.getText());
                    input_field.setText("");
                }
            }
        };
        but_div.addActionListener(div_listener);

        //===============================================================//

        ActionListener eq_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (label_3.getText() == "") {
                    label_3.setText(input_field.getText());
                    input_field.setText("");
                }

                label_4.setText("=");
                int a = Integer.parseInt(label_1.getText());
                int b = Integer.parseInt(label_3.getText());
                int c = 0;

                if (label_2.getText() == "+") c = a + b;
                if (label_2.getText() == "-") c = a - b;
                if (label_2.getText() == "*") c = a * b;
                if (label_2.getText() == "/") c = a / b;

                label_5.setText(String.valueOf(c));
            }
        };
        but_eq.addActionListener(eq_listener);

        //===============================================================//

        ActionListener clear_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                label_1.setText("");
                label_2.setText("");
                label_3.setText("");
                label_4.setText("");
                label_5.setText("");
                input_field.setText("");
            }
        };
        but_clear.addActionListener(clear_listener);

        //===============================================================//

        ActionListener pol_listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = input_field.getText(); // Исходная входная строка
                String str_1 = ""; // Исходная строка без пробелов
                String str_2 = ""; // Исходная строка с один пробелом между лексемами

                // Уберем все пробелы
                for (int i = 0; i < str.length(); i++) {
                    if (str.charAt(i) != ' ') str_1 += str.charAt(i);
                }

                for (int i = 0; i < str_1.length()-1; i++) {
                    if (str_1.charAt(i) > '0' && str_1.charAt(i) < '9') {
                        if (str_1.charAt(i+1) > '0' && str_1.charAt(i+1) < '9'){
                            // i-ый символ число и i+1-ый символ число
                            str_2 += str_1.charAt(i);
                        } else {
                            // i-ый символ число, а i+1 не число
                            str_2 += str_1.charAt(i);
                            str_2 += ' ';
                            str_2 += str_1.charAt(i+1);
                            str_2 += ' ';
                            i++;
                        }
                    } else {
                        str_2 += str_1.charAt(i) + " ";
                    }

                }
                if (str_1.charAt(str_1.length()-1) != ')' && str_1.charAt(str_1.length()-1) != '(')
                    str_2 += str_1.charAt(str_1.length()-1);
                if (str_1.charAt(str_1.length()-2) == ')' || str_1.charAt(str_1.length()-2) == '(')
                    str_2 += str_1.charAt(str_1.length()-1);

                String[] strs = str_2.split(" ");
                String out_str = "";
                char ch;
                Stack stack = new Stack(); // ������ ����
                HashMap<String,Integer> prior = new HashMap<String,Integer>();
                prior.put("+",1);
                prior.put("-",1);
                prior.put("*",2);
                prior.put("/",2);
                prior.put("(",0);
                prior.put(")",0);

                for (int i = 0; i < strs.length; i++)
                {
                    ch = strs[i].charAt(0);

                    if (ch != '+' && ch != '-' && ch != '*' && ch != '/' && ch != '(' && ch != ')') // ��� ����� �� 0 �� 9
                    {
                        out_str += strs[i] + ' ';
                        continue;
                    }

                    if (ch == '(')
                    {
                        stack.push(ch);
                        continue;
                    }

                    if (ch == ')')
                    {
                        if (stack.empty()) {
                            label_1.setText("Не согласованы скобки");
                            return;
                        }


                        while ((char)stack.peek() != '(')
                        {
                            out_str += stack.pop() + " ";
                            if (stack.empty()) {
                                label_1.setText("Не согласованы скобки");
                                return;
                            }
                        }
                        stack.pop();
                        continue;
                    }

                    if (ch == '+' || ch == '*' || ch == '-' || ch == '/')
                    {
                        if (!stack.empty())
                            if ((char)stack.peek() < '0' || (char)stack.peek() > '9') // ��� �� �����
                                while (!stack.empty() && prior.get(String.valueOf(ch)) <= prior.get(String.valueOf(stack.peek())))
                                {
                                        out_str += stack.pop();
                                        out_str += ' ';
                                }
                        stack.push(ch);
                    }
                }

                while (!stack.empty()) {
                    out_str += stack.pop();
                    out_str += ' ';
                }

                input_field.setText(out_str);

                strs = out_str.split(" ");
                for (int i = 0; i < strs.length; i++)
                {
                    ch = strs[i].charAt(0);
                    if (ch >= '0' && ch <= '9') // ������ ������ ����� => strs[i] - �����
                    {
                        stack.push(strs[i]);
                        continue;
                    }

                    // ch ���� +, ���� -, *, /
                    double b = Double.parseDouble((String)stack.pop()); // �������� ������ � �����
                    double a = Double.parseDouble((String)stack.pop()); // �������� ������ � �����
                    double c = 0;

                    if (ch == '+') c = a + b;
                    if (ch == '-') c = a - b;
                    if (ch == '*') c = a * b;
                    if (ch == '/') c = a / b;

                    stack.push(String.valueOf(c));
                }

                label_1.setText((String)stack.pop());

            }
        };
        but_pol.addActionListener(pol_listener);

        //===============================================================//

        frm.add(input_field);
        frm.add(but_plus);
        frm.add(but_minus);
        frm.add(but_mul);
        frm.add(but_div);
        frm.add(but_eq);
        frm.add(but_clear);
        frm.add(but_pol);
        frm.add(label_1);
        frm.add(label_2);
        frm.add(label_3);
        frm.add(label_4);
        frm.add(label_5);

        frm.setVisible(true);
    }


}
